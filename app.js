const express = require("express");
const cors = require("cors");
const mongoose = require('mongoose');
require('dotenv').config()
const logger = require('./logger');

const authRoutes = require('./app/routes/auth');
const user = require('./app/routes/user');
const post = require('./app/routes/post');
const app = express();

const corsOptions = {
  origin: "https://weblab-frontend.vercel.app",
  credentials: true,
};

app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/auth', authRoutes);
app.use('/user', user)
app.use('/post', post)

mongoose
  .connect(process.env.DB_CLOUD)
  .then(() => {
    console.log("Successfully connect to MongoDB.");
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

module.exports = app