/** @type {import('jest').Config} */
const config = {
  coverageReporters: ['json', 'lcov', ['text', {skipFull: true}]],
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!**/node_modules/**',
    '!**/vendor/**',
  ],
};

module.exports = config;