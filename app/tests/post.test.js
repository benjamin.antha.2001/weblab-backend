const mongoose = require("mongoose");
const request = require("supertest");
const databaseConfig = require("../config/database.config");
const app = require("../../app")

beforeEach(async () => {
    await mongoose.connect(process.env.DB_CLOUD)
})

afterEach(async () => {
    await mongoose.connection.close();
});

describe("GET /post/published", () => {
    it("should return all products", async () => {
      const res = await request(app).get("/post/published");
      expect(res.statusCode).toBe(200);
      expect(res.body.length).toBeGreaterThan(0);
    });
});

describe("GET /post/id", () => {
    it("should return one product", async () => {
      const id = "66210479851a863e99f08cfb"
      const res = await request(app).get("/post/" + id);
      expect(res.statusCode).toBe(200);
      expect(res.body._id).toBe(id);
    });
});