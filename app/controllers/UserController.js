const User = require('../models/user.model');

async function getAllUser(req, res) {
    try{
        const data = await User.find();
        res.json(data)
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
}

async function getUserByID(req, res) {   
    try{
        const data = await User.findById(req.user._id);
        res.json({username: data.username, role: data.role})
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
    
}

async function updateUserByID(req, res) {
    if (req.user._id == req.params.id){
        try {
            const id = req.params.id;
            const updatedData = req.body;
            const options = { new: true };

            const result = await User.findByIdAndUpdate(
                id, updatedData, options
            )
            res.send(result)
        }
        catch (error) {
            res.status(400).json({ message: error.message })
        }
    } else {
        res.status(403).json({ error: 'Not Allowed' });
    }
}

async function deleteUserByID(req, res) {
    if (req.user._id == req.params.id){
        try {
            const id = req.params.id;
            const data = await User.findByIdAndDelete(id)
            res.send(`Document with ${data.name} has been deleted..`)
        }
        catch (error) {
            res.status(400).json({ message: error.message })
        }
    } else {
        res.status(403).json({ error: 'Not Allowed' });
    }
}

module.exports = { 
    getAllUser,
    getUserByID,
    updateUserByID,
    deleteUserByID
 };