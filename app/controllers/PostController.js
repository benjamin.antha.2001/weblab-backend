const logger = require('../../logger');
const Post = require('../models/post.model');

const PUBLISHED = "PUBLISHED"
const DRAFT = "DRAFT"

const PUBLISHER = "PUBLISHER"

async function saveAPost(req, res) {
    if (req.user.role === PUBLISHER){
        try {
            const { title, description, content, place, state, isAnEvent, eventDate } = req.body;
            const post = new Post({ title, description, content, author: req.user.username, place, state, isAnEvent, date: new Date(), eventDate })
            if (post.state == PUBLISHED) {
                logger.info(req.user.username + " has posted a post with the title " + post.title);
            }
            await post.save();
            res.status(201).json({ message: 'Post saved successfully' });
        } catch (error) {
            console.log(error)
            res.status(500).json({ error: 'Saving post failed' });
        }
    } else {
        res.status(403).json({ error: 'You have to be a "Publisher" to save Posts' });
    }
}

async function getAllPublishedPosts(req, res) {
    try{
        const data = await Post.find({ state: PUBLISHED }).sort('-date');
        res.json(data)
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
}

async function getAllPostsByPublisher(req, res) {
    try{
        const data = await Post.find({author: req.user.username}).sort('-date');
        res.json(data)
    }
    catch(error){
        res.status(500).json({message: error.message})
    }
}

async function getPostByID(req, res) {
    try{
        const post = await Post.findById(req.params.id);
        if (post.state === "PUBLISHED") {
            res.json(post)
        } else {
            res.status(403).json({ error: 'No Post Found' });
        }
        

    }
    catch(error){
        res.status(500).json({message: error.message})
    }
    
}

async function getDraftByID(req, res) {
    try{
        const post = await Post.findById(req.params.id);
        if (post.state === "PUBLISHED") {
            res.status(403).json({ error: 'Already Published' });
        } else {
            if (req.user.username == post.author){
                res.json(post)
            } else {
                res.status(403).json({ error: 'Not Allowed' });
            }
        }
        

    }
    catch(error){
        res.status(500).json({message: error.message})
    }
}

async function updatePostByID(req, res) {
    try {
        let post = await Post.findById(req.params.id);
        if (req.user.username == post.author){ 
            const id = req.params.id;
            const updatedData = req.body;
            const options = { new: true };
    
            post = await Post.findByIdAndUpdate(
                id, updatedData, options
            )
            if (post.state == PUBLISHED) {
                logger.info(req.user.username + " has posted a post with the title " + post.title);
            }
            res.send(post)
        } else {
            res.status(403).json({ error: 'Not Allowed' });
        }
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
    
}

async function deletePostByID(req, res) {
    try {
        let post = await Post.findById(req.params.id);
        if (req.user.username == post.author){
            const id = req.params.id;
            const data = await Post.findByIdAndDelete(id)
            res.send(`Document with ${data._id} has been deleted..`)
        } else {
            res.status(403).json({ error: 'Not Allowed' });
        }
    }
    catch (error) {
        res.status(400).json({ message: error.message })
    }
    
}

module.exports = { 
    getAllPostsByPublisher,
    getPostByID,
    getDraftByID,
    updatePostByID,
    deletePostByID,
    getAllPublishedPosts,
    saveAPost
 };