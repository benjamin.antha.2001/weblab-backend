const express = require('express');
const verifyToken = require('../middlewares/verifyToken');
const verifyRole = require('../middlewares/verifyRole')

const PostController = require('../controllers/PostController')

const router = express.Router()

router.get('/published', PostController.getAllPublishedPosts)

router.get('/:id', PostController.getPostByID)

router.post('/', verifyToken, verifyRole, PostController.saveAPost)

router.get('/', verifyToken, verifyRole, PostController.getAllPostsByPublisher)

router.get('/draft/:id', verifyToken, verifyRole, PostController.getDraftByID)

router.patch('/:id', verifyToken, verifyRole, PostController.updatePostByID)

router.delete('/:id', verifyToken, verifyRole, PostController.deletePostByID)

module.exports = router;