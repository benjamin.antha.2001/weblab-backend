const express = require('express');
const router = express.Router();
const verifyToken = require('../middlewares/verifyToken');

const AuthController = require('../controllers/AuthController')


// User registration
router.post('/register', AuthController.registerUser);

// User login
router.post('/login', AuthController.loginUser);

// User logout
router.get('/logout', AuthController.logoutUser);

module.exports = router;