const express = require('express');
const verifyToken = require('../middlewares/verifyToken');

const UserController = require('../controllers/UserController')

const router = express.Router()

router.get('/', verifyToken, UserController.getUserByID)

//Get by ID Method
router.get('/:id', verifyToken, UserController.getUserByID)

//Update by ID Method
router.patch('/:id', verifyToken, UserController.updateUserByID)

//Delete by ID Method
router.delete('/:id', verifyToken, UserController.deleteUserByID)

module.exports = router;