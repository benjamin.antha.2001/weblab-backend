const mongoose = require('mongoose');

const user = new mongoose.Schema({
    username: {
        unique: true,
        required: true,
        type: String
    },
    password: {
        required: true,
        type: String
    },
    role: {
        required: true,
        type: String,
    }
})

module.exports = mongoose.model('User', user)