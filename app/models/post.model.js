const mongoose = require('mongoose');

const post = new mongoose.Schema({
    title: {
        unique: true,
        required: true,
        type: String
    },
    description: {
        required: true,
        type: String
    },
    content: {
        required: true,
        type: String,
    },
    author: {
        required: true,
        type: String
    },
    date: Date,
    place: String,
    state: String,
    isAnEvent: Boolean,
    eventDate: Date,
})

module.exports = mongoose.model('Post', post)