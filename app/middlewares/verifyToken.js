const jwt = require('jsonwebtoken');
const Blacklist = require('../models/blacklist.model');
require('dotenv').config()

async function verifyToken(req, res, next) {
    try {
        const authHeader = req.headers["cookie"];

        if (!authHeader) return res.sendStatus(401);
        const cookie = authHeader.split("=")[1];

        const accessToken = cookie.split(";")[0];

        const checkIfBlacklisted = await Blacklist.findOne({ token: accessToken }); // Check if that token is blacklisted
        // if true, send an unathorized message, asking for a re-authentication.
        if (checkIfBlacklisted)
            return res
                .status(401)
                .json({ message: "Session expired" });

        jwt.verify(accessToken, process.env.JWT_SECRET_KEY, async (err, decoded) => {
            if (err) {
                return res
                    .status(401)
                    .json({ message: "Session expired" });
            }

            req.user = decoded.user;
            next();
        });
    } catch (err) {
        res.status(500).json({
            status: "error",
            code: 500,
            data: [],
            message: "Internal Server Error",
        });
    }
}

module.exports = verifyToken;